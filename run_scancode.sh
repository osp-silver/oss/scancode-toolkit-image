#!/usr/bin/env bash
# Scans code for licenses.

. venv-scancode/bin/activate

# Extract parameters for extractcode

POSITIONAL=()
EXTRACTCODE_POSITIONAL=("--quiet")
while [[ $# -gt 0 ]]; do
key="$1"

case $key in
    --shallow)
    EXTRACTCODE_POSITIONAL+=("$1")
    shift
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

/app/venv-scancode/bin/extractcode ${EXTRACTCODE_POSITIONAL[@]} /scan

# Scan and output pretty print JSON on STDOUT
/app/venv-scancode/bin/scancode ${POSITIONAL[@]}
