FROM python:3.12-slim as build

ENV DEBIAN_FRONTEND=noninteractive
# Workaround OSP firewall blocks downloads
RUN echo 'Acquire::Retries "10";' > /etc/apt/apt.conf.d/80-retries
# Requirements as per https://scancode-toolkit.readthedocs.io/en/latest/getting-started/install.html
RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y build-essential pkg-config bzip2 xz-utils zlib1g libxml2-dev libxslt1-dev \
 && apt autoremove -y --purge \
 && rm -rf /var/cache/apt/archives/* \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
WORKDIR /app

# Create virtual environment.
# Install scancode-toolkit.
# Install extractcode separately
# Install 'pyicu' for better text transliteration.
# Run scancode-reindex-licenses once for initial configuration to create the base license index
RUN python -m venv venv-scancode \
    && . venv-scancode/bin/activate \
    && pip install --upgrade pip \
    && pip install wheel \
    && pip install scancode-toolkit[full] extractcode[full] pyicu \
    && /app/venv-scancode/bin/scancode-reindex-licenses

FROM python:3.12-slim as final

ENV DEBIAN_FRONTEND=noninteractive
# Workaround OSP firewall blocks downloads
RUN echo 'Acquire::Retries "10";' > /etc/apt/apt.conf.d/80-retries
# Requirements as per https://scancode-toolkit.readthedocs.io/en/latest/getting-started/install.html
RUN apt-get update \
 && apt-get upgrade -y \
 && apt-get install -y libgomp1 bzip2 xz-utils zlib1g libxml2-dev libxslt1-dev \
 && apt autoremove -y --purge \
 && rm -rf /var/cache/apt/archives/* \
 && rm -rf /var/lib/apt/lists/*

RUN mkdir /app
RUN mkdir /scan
WORKDIR /app

# Copy installed environment with program
COPY --from=build /app /app

COPY run_scancode.sh /usr/local/bin/run_scancode.sh
COPY docker_image_scancode_toolkit.sh /usr/local/bin/docker_image_scancode_toolkit.sh
COPY Dockerfile.for_scan /usr/local/src/Dockerfile.for_scan

ENTRYPOINT ["run_scancode.sh"]
CMD ["-lp", "-n", "4", "--ignore", "*.o", "--ignore", "*~", "--json-pp", "-", "--quiet", "/scan", "--summary", "--classify"]
