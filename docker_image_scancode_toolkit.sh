#!/usr/bin/env sh
# Scans licenses.
#
# Requires write permissions in the directory with this script to create tar archive.
rc=0

if [ -z "${docker_image}" ]; then
  echo "Error: Provide environment variable docker_image"
  exit 1
fi
if [ -z "${scancode_toolkit_image}" ]; then
  scancode_toolkit_image=registry.gitlab.com/osp-silver/oss/scancode-toolkit-image:latest
fi
if [ -n "${search_path}" ]; then
  # Remove / at the end before appending /*
  search_path_opt="-path \"$(echo "${search_path}" | sed 's/\/$//')/*\""
else
  search_path_opt=''
fi

# Create temporary directory for archive file inside the context of the build
# and prepare removing this data in case of an error.
tmpdir=
cleanup () {
  if [ -n "$tmpdir" ] ; then rm -rf "$tmpdir"; fi
  if [ -n "$1" ]; then kill -$1 $$; fi
}
tmpdir=$(mktemp -d -p .)
trap 'cleanup' EXIT
trap 'cleanup HUP' HUP
trap 'cleanup TERM' TERM
trap 'cleanup INT' INT
tmp_archive="$tmpdir/for_scan.tar.gz"

# Create container from image to be checked,
# export files as compressed tar ignoring errors
#
# Do not follow symlinks when searching from "/".
#
# Do not pass "/" to tar. This would archive all files.

cmd_tar=`cat <<CMD_TAR
find / \\( \
-path "/boot" -type d -prune -o \
-name "cache" -type d -prune -o \
-path "/dev" -type d -prune -o \
-path "/media" -type d -prune -o \
-path "/mnt" -type d -prune -o \
-path "/proc" -type d -prune -o \
-path "/run" -type d -prune -o \
-path "/sys" -type d -prune -o \
-name "tmp" -type d -prune -o \
-path "/var" -type d -prune \
  \\) -o -type f ${search_path_opt} -exec tar -cz --no-recursion '{}' \+ \
2>/dev/null
CMD_TAR
`

# Create archive with files for scan
docker run --rm --entrypoint sh ${docker_image} -c "${cmd_tar}" > "${tmp_archive}"
tmp_rc=$?
if [ $rc -eq 0 ] && [ $tmp_rc -gt 0 ]; then
  # Keep the error code
  rc=$tmp_rc
fi

# Extract archive inside a test image
test_image="$(docker build \
--build-arg SCANCODE_TOOLKIT_IMAGE="${scancode_toolkit_image}" \
--build-arg ARCHIVE_PATH="${tmp_archive}" \
--quiet \
-f Dockerfile.for_scan .)"
tmp_rc=$?
if [ $rc -eq 0 ] && [ $tmp_rc -gt 0 ]; then
  # Keep the error code
  rc=$tmp_rc
fi

# Delete archive to save memory
rm "${tmp_archive}"
tmp_rc=$?
if [ $rc -eq 0 ] && [ $tmp_rc -gt 0 ]; then
  # Keep the error code
  rc=$tmp_rc
fi

# Run test
if [ -n "${scancode_toolkit_options}" ]; then
  docker run --rm "${test_image}" ${scancode_toolkit_options}
else
  docker run --rm "${test_image}"
fi
tmp_rc=$?
if [ $rc -eq 0 ] && [ $tmp_rc -gt 0 ]; then
  # Keep the error code
  rc=$tmp_rc
fi

# Remove test image
docker image rm "${test_image}" > /dev/null
tmp_rc=$?
if [ $rc -eq 0 ] && [ $tmp_rc -gt 0 ]; then
  # Keep the error code
  rc=$tmp_rc
fi

exit $rc
